<?php

/**
 * @file
 * Administrative page callbacks for the facebox module.
 */

/**
 * General configuration form for controlling the facebox behaviour.
 */
function facebox_admin_settings() {



  $form['facebox_advanced_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Usage settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('To activate the popup of HTML (facebook style) add the RELATIVE "facebox" tag to you link.<br>example: &lt;a href="" rel="facebox"&gt;<br>
	<br>This will render the linked page (node content only) into a clean facebox template.<br><br> To use your own theme template file (e.g page-node.tpl.php), use RELATIVE "custom-facebox."'),

  );

  return system_settings_form($form);
}
